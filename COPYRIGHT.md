# COPYRIGHT

Copyright (C) 2005-2024, LEGI UMR 5519 / CNRS UGA G-INP, Grenoble, France
 http://www.legi.grenoble-inp.fr

Gestex est une application écrite en PHP utilisant une base de données MySQL.
Elle permet de faire inventaire du matériel d'expérimentation d'un lanoratoire
et d'en gérer les éventuels réservations et prêts.

## SEE ALSO

 * Gestex - https://gricad-gitlab.univ-grenoble-alpes.fr/legi/soft/gestex
 * LICENSE - https://www.gnu.org/licenses/agpl-3.0.en.html
 * AUTHORS.md
 * README.md
