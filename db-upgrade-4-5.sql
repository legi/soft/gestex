--
-- VERSION : 5, DATE : 09/06/2021
-- AUTHOR : RISTICH Estéban	
--

--
-- Upgrade table
--

ALTER TABLE `Listing` MODIFY COLUMN `barcode` BIGINT DEFAULT NULL;
ALTER TABLE `Listing` ADD COLUMN `max_day` INT(11) NOT NULL DEFAULT 0;
UPDATE `Listing` SET `max_day` = 0;


-- TIMESTAMP =< MySQL 5.5.x < DATETIME
ALTER TABLE `version` ADD COLUMN `updated_on` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;


ALTER TABLE `pret` ADD COLUMN `status` ENUM('LOAN_BORROWED', 'LOAN_RESERVED', 'LOAN_RETURNED') NOT NULL;
UPDATE `pret` SET `status` = 'LOAN_BORROWED';


--
-- ADD TABLE
--

RENAME TABLE `intervention` TO `old_intervention`;

CREATE TABLE IF NOT EXISTS `intervention` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `supplier_id` INT(11) NOT NULL,
  `equipment_id` INT(11) NOT NULL,
  `description` VARCHAR(255) DEFAULT NULL,
  `date` DATE NOT NULL DEFAULT CURRENT_DATE,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`supplier_id`) REFERENCES `fournisseurs` (`id`),
  FOREIGN KEY (`equipment_id`) REFERENCES `Listing` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `recipe` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `pathname` VARCHAR(500) DEFAULT NULL,
  `description` VARCHAR(150) NOT NULL,
  `intervention_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`intervention_id`) REFERENCES `intervention` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 CHARSET=utf8;


--
-- Fix global DB version
--

UPDATE `version` SET `version` = 5 WHERE `soft` = 'database';
