<?php if (!$web_page) exit() ?>

<?php
// $message_alert
?>

<?php en_tete('Erreur avec la base de donn&eacute;es') ?>

<div class="box-alert">
	<center>
		Une erreur est survenue lors de l'acc&egrave;es aux donn&eacute;es avec la base de donn&eacute;e.
		<br>
		<b><?=$message_alert?></b>
		<br>
		Veuillez voir avec votre administrateur pour vous corriger &eacute;ventuellement les probl&egrave;mes sur votre base de donn&eacute;es.
		En cas d'erreurs syst&eacute;matiques, veuillez remonter un bogue aux d&eacute;veloppeurs amont.
	</center>
</div>

<?php pied_page() ?>
