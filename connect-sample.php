<?php if (!$web_page) exit() ?>

<?php
// connect-sample.php

// Do not modify directly this file
// Rename and adapt this file under the name connect.php

exit(); // Comment this line

// Informations de connexions a la base mySQL
define('GESTEX_DB_USER',     "gestex-service");
define('GESTEX_DB_PASSWORD', "gestex-magic-password");
define('GESTEX_DB_SERVER',   "localhost");
define('GESTEX_DB_DATABASE', "gestex");

// Parametres generaux
define('GESTEX_ADMIN_MAIL',  "webmaster@your-entity.sample");
define('GESTEX_ENTITY_NAME', "YOUR ENTITY");
define('GESTEX_ENTITY_URL',  "http://www.your-entity.sample/");
define('GESTEX_ENTITY_LOGO', "your-entity.jpg");
?>
