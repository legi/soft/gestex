# AUTHORS

 * Pierre Carecchio <Pierre.Carecchio(A)neel.cnrs.fr>
 * Muriel Lagauzere <Muriel.Lagauzere(A)univ-grenoble-alpes.fr>
 * Gabriel Moreau <Gabriel.Moreau(A)univ-grenoble-alpes.fr>
 * Sylène Moreau <Sylene.Moreau(A)isen-ouest.yncrea.fr>
 * Olivier De Marchi <Olivier.De-Marchi(A)univ-grenoble-alpes.fr>
 * Estéban Ristich <Esteban.Ristich(A)protonmail.com>
