--
-- VERSION : 2021, DATE : 09/06/2021
-- AUTHOR : RISTICH Estéban	
--

-- Ceci est la proposition d'un nouveau schema de base
-- de donnée construite de zéro qui supportera les relations.
-- Le nouveau schema essaiera de suivre au mieux
-- les principes récent de la norme SQL telle que la
-- nomination des colonnes (ex: nomination anglophone,
-- nomination au pluriel des tables, type BOOLEAN commencant
-- par "is_*", clés étrangères commancant par "f_*", 
-- type DATE terminant par "*_at" et autres ...)


-- SCHEMA
-- END SCHEMA