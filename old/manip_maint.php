<?php

//manip_maint.php

// Authenticate
require_once('module/auth-functions.php');

if (!auth(1))
	Header("Location: login.php");

$logged_id        = $_SESSION['logged_id'];
$logged_user = strtolower($_SESSION['logged_user']);
$logged_level     = $_SESSION['logged_level'];

//recuper la methode de tri
if (empty($_GET['tri']))
	$tri = 'nom';
else
	$tri = $_GET['tri'];

//et le numero de manip
if (empty($_GET['id']))
	Header("Location: list_manip.php");
else
	$manip_id=$_GET['id'];

require_once('module/html-functions.php');

en_tete('Historique Manip');
if ($pdo = connect_db()){

	// recupere les refs du user
	$sql = 'SELECT prenom, nom FROM users WHERE loggin = ?;';
	// list($qh,$num) = query_db($querry);

	// $data = result_db($qh);
	$stmt = $pdo->prepare($sql);
	$stmt->execute(array($logged_user));
	$user = $stmt->fetchAll(PDO::FETCH_ASSOC);
	// echo " Bienvenue ";
	// var_dump($user);
	// echo $[0]]['prenom'];
	// echo $user[0]['nom'];
	// echo " ($logged_id)<br /><br />";
?>

<br />
Voici la liste des Projets de la manip :<br />
<?php
$sql = 'SELECT * FROM manip WHERE id = ?;';
//  list($qh,$num) = query_db($querry);
///recupere les infos de la manip
//  $data = result_db($qh);
$stmt = $pdo->prepare($sql);
$stmt->execute(array($manip_id));
$manip = $stmt->fetchAll(PDO::FETCH_ASSOC);
$dossier_manip=$manip[0]['nom'];
?>

<!-- titre -->
<table cellpadding="1" cellspacing="1" border=1 style="width: 90%; text-align: center; margin-left: auto; margin-right: auto;">
	<tbody>
		<tr bgcolor="#f7d709">
			<?php
			echo "<td rowspan=2><h2>".$manip[0]['nom']." (".$manip[0]['id'].")</h2> <i>Date</i> : ".$manip[0]['date']."<br /></td>".PHP_EOL;
			echo "<td style=\" text-align: left;\">".$manip[0]['descr']."<br /></td>".PHP_EOL;
			?>
		</tr>

		<tr bgcolor="#f7d709">
			<td style="text-align: center;">
			<?php
			// recupere le nom de de equipes
			$sql = 'SELECT nom FROM equipe WHERE id = ?;';
			// list($qheq,$numeq) = query_db($querry);
			// $eq = result_db($qheq)  ;
			$stmt = $pdo->prepare($sql);
			$stmt->execute(array($manip[0]['equipe']));
			$eq = $stmt->fetchAll(PDO::FETCH_ASSOC);
			if (!empty($eq)) {echo '<i>&Eacute;quipe</i> : '.$eq[0]['nom'].'<br />';}
			// recupere le nom du chercheur
			$sql = 'SELECT nom FROM users WHERE id = ?;';
			// list($qheq,$numeq) = query_db($querry);
			// $eq = result_db($qheq)  ;
			$stmt = $pdo->prepare($sql);
			$stmt->execute(array($manip[0]['chercheur']));
			$eq = $stmt->fetchAll(PDO::FETCH_ASSOC);
			if (!empty($eq)) {echo '<i>Chercheur</i> : '.$eq[0]['nom'].'<br />';}
			if (!empty($manip)) {echo '<i>Local</i> : '.$manip[0]['local'].'<br />';}
			?>
			</td>
		</tr>
	</tbody>
</table>
<br />

<!-- menu commandes -->
<table cellpadding="1" cellspacing="1" border="0"
	style="width: 90%; text-align: left; margin-left: auto; margin-right: auto;">
	<tbody>
		<tr class="menu">
			<?php if ($logged_level >= 2) { ?>
			<td style="vertical-align: top; text-align: center;">
				<a href="add_proj.php?idm=<?php echo $manip_id ?>">Ajout d'un Projet</a>
				<br />
			</td>
			<td style="vertical-align: top; text-align: center;">
				<a href="assoc_proj.php?id=<?php echo $manip_id ?>">Association d'un Projet</a>
				<br />
			</td>
			<?php } ?>
			<td style="vertical-align: top; text-align: center;">
				<a href="list_manip.php">Retour &agrave; l'accueil</a>
				<br />
			</td>
			<td style="vertical-align: top; text-align: center;">
				<a href="logout.php?variable=projet">Quitter</a>
				<br />
			</td>
		</tr>
	</tbody>
</table>
<br />

<!-- tableau(x) des projets -->
<?php
 // un tableau par projet, contenant les taches...
 $total_manip= 0;$total_projet=0;
 $sql = 'SELECT id, nom FROM projet WHERE manip = ? ORDER BY date' ;
//  list($qh,$num) = query_db($querry);
 ///recupere les infos de la manip
//  while ($manips = result_db($qh)){
$stmt = $pdo->prepare($sql);
$stmt->execute(array($manip_id));
$projet = $stmt->fetchAll(PDO::FETCH_ASSOC);

  foreach($projet as $manips){
 $proj_id=$manips['id'];
 ?>

<script language="javascript">
function windowToTop(lien){
	wnd = window.open(lien, 'Project Info', 'location=0,directories=no,status=no,menubar=yes,resizable=1,width=550,height=550');
	wnd.focus();
}
</script>

<table cellpadding="1" cellspacing="1" border="1" style="width: 90%; text-align: left; margin-left: auto; margin-right: auto;">
	<tbody>
		<tr bgcolor="#f7bb09">
			<th style="vertical-align: top; text-align: left;" colspan="4" >
				<?php  echo "Projet : <a href=\"#\" onclick=\"windowToTop('proj_info.php?idm=". $manip_id ."&idp=". $proj_id ."');\" title =\"D&eacute;tails de ce projet\">";
				echo $manips['nom']." (".$manips['id'].")"; ?> </a><br />
			</th>
			<th style="vertical-align: top; text-align: left;">
 <?php
 ///bouton lien vers la doc
 $dossier_proj ="data/".$dossier_manip."/". $manips['nom'];
 //remplace les espaces par des underscore
 $dossier_proj = str_replace(" ", "_", $dossier_proj);
 // cherche l'existence de ce dossier
 ///echo $dossier_proj;
 /// @ devant la fonction pour eviter d'avoir un message d'erreur sur la page web, s'il n'y a pas de dossier
 if (@opendir($dossier_proj) != FALSE){
  //si trouve ajoute un bouton
  echo "Voir : <a href =\"browse_proj.php?idm=". $manip_id ."&idp=".$proj_id."\">".ICON_SEE_DOC."</a><br />";

 }
 ?>
  </th>
<?php if ($logged_level >= 2) { ?>
  <!-- <th colspan="3"></th> //ajout de doc   -->
    <th style="vertical-align: top; text-align: left;">

  <a href ="add_doc.php?idm=<?php echo $manip_id ?>&idp=<?php echo $proj_id ?>"><?php echo ICON_ADD_DOC ?></a><br />
  </th>
   <th style="vertical-align: top; text-align: right;">
  <a href ="add_proj.php?idm=<?php echo $manip_id ?>&idp=<?php echo $proj_id ?>"><?php echo ICON_EDIT ?></a><br />
   </th>
    <th style="vertical-align: top; text-align: right;">
  <a href ="del_proj.php?idm=<?php echo $manip_id ?>&idp=<?php echo $proj_id ?>"><?php echo ICON_TRASH ?></a><br />
   </th>

    <?php } else {
  echo "<th colspan=\"2\"></th>";
  } ?>
 </tr>
 <!-- <tr bgcolor="#f7d709">
  <th style="vertical-align: top; text-align: center;">T&acirc;ches<br />      </th>
  <th style="vertical-align: top; text-align: center;">Debut :<br />      </th>
  <th style="vertical-align: top; text-align: center;">Par :<br />      </th>
  <th style="vertical-align: top; text-align: center;">Temps pass&eacute;:<br />      </th>

 <?php if ($logged_level >= 2) { ?>
  <th colspan="2" style="vertical-align: top; text-align: right;" >
  <a href ="add_task.php?idm=<?php echo $manip_id ?>&idp=<?php echo $proj_id ?>">Ajouter une tache</a><br />
    </th>
 <?php }

  else {
  echo "<th colspan=\"2\"></th>";
  } ?>
 </tr> --->
<?php //interrogation base de donnees
 $total_projet=0;
 // recupere la liste des taches de ce projet
 $sql = 'SELECT id, nom, date, user, temps FROM tache WHERE projet = ? ORDER BY date;';
//  list($qh2,$num2) = query_db($querry);
//  while ($taches = result_db($qh2)) {
  $stmt = $pdo->prepare($sql);
  $stmt->execute(array($proj_id));
  $tache = $stmt->fetchAll(PDO::FETCH_ASSOC);
  foreach($tache as $taches){
  // remplit le tableau de taches
       echo"<tr><td>".ICON_MARK_RIGHT."</td>";
  echo"<td style=\"vertical-align: top;\">";
  ///     echo "<a href=\"task_info.php?idm=".$manip_id."&idp=".$proj_id."&idt=".$taches[id]."\" target=\"new\">".$taches[nom]."</a>";
     echo " <a href=\"#\" onclick=\"windowToTop('task_info.php?idm=".$manip_id."&idp=".$proj_id."&idt=". $taches['id']."');\">";
 echo $taches['nom']."</a>";
  echo"</td><td style=\"vertical-align: top;\">";
        echo "date:".$taches['date'];
     echo"</td><td style=\"vertical-align: top;\">";

  ///interro db table temps
  $sql = 'SELECT duree, user FROM temps WHERE id_tache = ?;';
  // list($qh4,$num4) = query_db($querry);
  $temps_tache=0;
  $users = "par: ";
  // while($temps = result_db($qh4)){
  $stmt = $pdo->prepare($sql);
  $stmt->execute(array($taches['id']));
  $temps = $stmt->fetchAll(PDO::FETCH_ASSOC);
  foreach($temps as $temps){
   $temps_tache+= $temps['duree'];

   //recupre le nom du user associe a ce temps
   $sql = 'SELECT nom FROM users WHERE id = ?;';
  //  list($qh3,$num3) = query_db($querry);
  //  $next_user= result_db($qh3 );
  $stmt = $pdo->prepare($sql);
  $stmt->execute(array($temps['user']));
  $next_user = $stmt->fetchAll(PDO::FETCH_ASSOC);
  if(!empty($next_user)){
   if ( strstr($users, $next_user[0]['nom'])==FALSE)
   // si ce nom n'est pas deja dans la chaine
     $users.= $next_user[0]['nom'].", ";
  }
  // $users=$users[,-1];
  echo $users;
  $users='';
}

       echo"</td><td style=\"vertical-align: top;\">";
   echo "dur&eacute;e :";
  echo $temps_tache." heures";
  $total_projet += $temps_tache;
   if ($logged_level >= 2) {
   echo "<a href=\"add_time.php?idm=".$manip_id."&idp=".$proj_id."&idt=".$taches['id']."\">".ICON_ADD_TIME.'</a>';
     echo"</td><td style=\"vertical-align: top;\">";
   // ajout d'un document a une tache -->
    echo "<a href=\"add_doc.php?idm=".$manip_id."&idp=".$proj_id."&idt=".$taches['id']."\">".ICON_ADD_DOC.'</a>';
     echo"</td><td style=\"vertical-align: top;\">";
       //modif d'une tache
    echo "<a href=\"add_task.php?idm=".$manip_id."&idp=".$proj_id."&idt=".$taches['id']."\">".ICON_EDIT."</a>";
   echo"</td><td style=\"vertical-align: top;\">";
  //supression dune tache
   echo "<a href=\"del_task.php?idm=".$manip_id."&idt=".$taches['id']."\">".ICON_TRASH."</a>";
  }
   echo"</td></tr>";

 }//end while taches

   echo"<tr><td style=\"vertical-align: top;text-align: left;\" >";
 if ($logged_level >= 2) {
 echo" <a href =\"add_task.php?idm=".$manip_id." ?>&idp=". $proj_id ."?>\">".ICON_ADD_TASK.'</a><br />';
 }

echo"</td><td style=\"vertical-align: top;text-align: right;\" colspan=3 >";
        echo "temps total :";
      echo"</td><td style=\"vertical-align: top;\">";
  echo $total_projet;
      echo" heures</td></tr>";
 echo"</tbody></table>";
 $total_manip+=$total_projet;
}//end while manip
echo "<br />temps total manip : ".$total_manip." heures<br /><br />";

////////projets associes

if (!empty($manip[0]['assoc_proj'])) {
	echo '<h3>Projet(s) associ&eacute;(s) :</h3>';
	echo '<ul>';
	$assoc = explode(',', $manip[0]['assoc_proj']);
	foreach($assoc as $a){
		// recupere l'identite du projet associe
		$sql = 'SELECT id, nom FROM projet WHERE id = ?;';
		// list($qh3,$num3) = query_db($querry);
		// $projet_a = result_db($qh3);
		$stmt = $pdo->prepare($sql);
		$stmt->execute(array($a));
		$projet_a = $stmt->fetchAll(PDO::FETCH_ASSOC);

		//liens vers ces projets
		echo '  <li>(".$a.") ';
		echo '    <a href="#" onclick="windowToTop(\'proj_info.php?idp='. $projet_a[0]['id'] .'\')" title ="D&eacute;tails de ce projet">';
		echo      $projet_a[0]['nom'];
		echo '    </a>';
		echo '  </li>';
	}
	echo '</ul>';
}

} // end if connect
?>

<br />
</div>
<?php pied_page(); ?>
