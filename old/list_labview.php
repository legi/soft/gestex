<?php
// list_labview.php
$web_page = true;

require_once('module/auth-functions.php');
require_once('module/html-functions.php');

$logged_id    = $_SESSION['logged_id'];
$logged_user  = strtolower($_SESSION['logged_user']);
$logged_level = $_SESSION['logged_level'];

en_tete('Liste des programmes Labview');

//recuper la methode de tri
$tri = $_GET[tri];
if (empty($tri))
	$tri ="id";
?>

<br />
<table cellpadding="2" cellspacing="2" border="1"
 style="width: 90%; text-align: left; margin-left: auto; margin-right: auto;">
  <tbody>
    <tr>
 <td style="vertical-align: top; text-align: center;">
	<a href="add_labview.php">Ajouter<br />une manip Labview</a>
	<br /></td>

<?php if ($logged_level >= 2) {
?>

 <td style="vertical-align: top; text-align: center;">
	<a href="logout.php?variable=instru">Quitter</a>
<?php }	?>

	<br /></td> </tr></tbody>
</table>

<br />
Liste des manip labview en cours : <br />

<table cellpadding="2" cellspacing="2" border="1"
 style="width: 90%; text-align: left; margin-left: auto; margin-right: auto;">
  <tbody>
    <tr bgcolor="#f7d709">

 <th style="vertical-align: top; text-align: center;">
	Manip + chercheur<br />
      </th>

<th style="vertical-align: top; text-align: center;">
	Developpeur<br />
      </th>

      <th style="vertical-align: top; text-align: center;">
	 Salle de la manip<br />
      </th>
     <th style="vertical-align: top; text-align: center;">
	Mat&eacute;riel d'acquisition ou de commande<br />
      </th>

 <th style="vertical-align: top; text-align: center;">
	Descriptif du code<br />
      </th>

      <th style="vertical-align: top; text-align: center;">
	Driver d'instrument<br />
      </th>

<th style="vertical-align: top; text-align: center;">
	Module sp&eacute;cifique Labview<br />
      </th>

<th style="vertical-align: top; text-align: center;">
	Impression &eacute;cran+doc pdf manip<br />
      </th>

<?php if ($logged_level >= 2)
		echo "</th><th>";
	if ($logged_level >= 3)
		echo "</th><th>";
	  ?>
    </tr>

<?php	//interrogation base de donnees

if ( $connex = connect_db() ){

	$querry = "SELECT * FROM labview ORDER BY '$tri'";
	list($qh,$num) = query_db($querry);

	$last_id=0;
}

$data = result_db($qh);

echo "<tr>";
 echo"</td><td style=\"vertical-align: top;\">";
	echo $data['manipch'];

       echo"</td><td style=\"vertical-align: top;\">";
echo $data['technicien'];

  echo"</td><td style=\"vertical-align: top;\">";

echo $data['localisation'];

  echo"</td><td style=\"vertical-align: top;\">";
echo $data['matos'];
 echo"</td><td style=\"vertical-align: top;\">";
echo $data['code'];
 echo"</td><td style=\"vertical-align: top;\">";
echo $data['driver'];
 echo"</td><td style=\"vertical-align: top;\">";
echo $data['module'];
 echo"</td><td style=\"vertical-align: top;\">";

$dossier_lab ="data/labview/".$data['manipch'];

	//remplace les espaces par des underscore
	$dossier_lab = str_replace(" ", "_", $dossier_lab);
	// cherche l'existence de ce dossier
	//echo $dossier_lab;
	/// @ devant la fonction pour eviter d'avoir un message d'erreur sur la page web, s'il n'y a pas de dossier
	if (@opendir($dossier_lab) != FALSE){
		//si trouve ajoute un bouton
		echo 'Voir : <a href ="doc_labview.php?id='.$data['id'].'">'.$data['manipch'].' '.ICON_SEE_DOC.'</a><br />';
    }

      echo"</td><td style=\"vertical-align: top;\">";
      echo "<a href=\"add_labview.php?id=$data['id']\">".ICON_EDIT."</a>";
      echo"</td>";

      echo"</td><td style=\"vertical-align: top;\">";
      echo "<a href=\"del_labview.php?id=$data['id']\">".ICON_TRASH."</a>";
      echo"</td>";

echo"</tr>";

while ($data = result_db($qh)) {

	// remplit le tableau

     echo"<tr><td style=\"vertical-align: top;\">";
echo $data['manipch'];
 echo"</td><td style=\"vertical-align: top;\">";
echo $data['technicien'];
echo"</td><td style=\"vertical-align: top;\">";
echo $data['localisation'];
echo"</td><td style=\"vertical-align: top;\">";

echo $data['matos'];
 echo"</td><td style=\"vertical-align: top;\">";
echo $data['code'];
 echo"</td><td style=\"vertical-align: top;\">";
echo $data['driver'];

 echo"</td><td style=\"vertical-align: top;\">";
echo $data['module'];

 echo"</td><td style=\"vertical-align: top;\">";

///bouton lien vers la doc
	$dossier_lab ="data/labview/".$data['manipch'];

	//remplace les espaces par des underscore
	$dossier_lab = str_replace(" ", "_", $dossier_lab);
	// cherche l'existence de ce dossier
	//echo $dossier_lab;
	/// @ devant la fonction pour eviter d'avoir un message d'erreur sur la page web, s'il n'y a pas de dossier
	if (@opendir($dossier_lab) != FALSE){
		//si trouve ajoute un bouton
		echo 'Voir : <a href ="doc_labview.php?id='. $data['id'].'">'.$data['manipch'].' '.ICON_SEE_DOC.'</a><br />';
    }
//echo $data['ecran'];

      echo"</td><td style=\"vertical-align: top;\">";
      echo "<a href=\"add_labview.php?id=$data['id']\">".ICON_EDIT.'</a>';
      echo"</td>";

      echo"</td><td style=\"vertical-align: top;\">";
      echo "<a href=\"del_labview.php?id=$data['id']\">".ICON_TRASH.'</a>';
      echo"</td>";

echo"</tr>";

	}//end while

?>

  </tbody>
</table>
<br />
</div>
<?php pied_page() ?>
